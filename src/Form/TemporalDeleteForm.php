<?php

/**
 * @file
 * Contains \Drupal\temporal\Form\TemporalDeleteForm.
 */

namespace Drupal\temporal\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Temporal entities.
 *
 * @ingroup temporal
 */
class TemporalDeleteForm extends ContentEntityDeleteForm {


}
