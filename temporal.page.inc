<?php

/**
 * @file
 * Contains temporal.page.inc..
 *
 * Page callback for Temporal entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Temporal templates.
 *
 * Default template: temporal.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_temporal(array &$variables) {
  // Fetch Temporal Entity Object.
  $temporal = $variables['elements']['#temporal'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
* Prepares variables for a custom entity type creation list templates.
*
* Default template: temporal-content-add-list.html.twig.
*
* @param array $variables
*   An associative array containing:
*   - content: An array of temporal-types.
*
* @see block_content_add_page()
*/
function template_preprocess_temporal_content_add_list(&$variables) {
  $variables['types'] = array();
  $query = \Drupal::request()->query->all();
  foreach ($variables['content'] as $type) {
    $variables['types'][$type->id()] = array(
      'link' => Link::fromTextAndUrl($type->label(), new Url('entity.temporal.add_form', array(
        'temporal_type' => $type->id()
      ), array('query' => $query))),
      'description' => array(
      '#markup' => $type->label(),
      ),
      'title' => $type->label(),
      'localized_options' => array(
      'query' => $query,
      ),
    );
  }
}
